terraform {
  required_version = "> 0.8.0"
}

resource "null_resource" "runner1" {

 provisioner "local-exec" {
    
    command = "/bin/bash run1.sh"
  }
}

resource "null_resource" "runner2" {

 provisioner "local-exec" {
    
    command = "/bin/bash run2.sh"
  }
}
resource "null_resource" "runner3" {

 provisioner "local-exec" {
    
    command = "/bin/bash run3.sh"
  }
}